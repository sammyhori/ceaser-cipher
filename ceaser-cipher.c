#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static char* charArrayToUpper(char charArray[128]) {
	unsigned int c = 0;
	while(charArray[c] != 0){
		if(charArray[c] >= 97 && charArray[c] <= 122){
			// Lowercase character
			charArray[c] -= 32;
		}
		c++;
	}
	return charArray;
}

static int processKey(int key, bool isEncrypt)
{
	while(key < 0){
		key += 26;
	}

	key = key % 26;
	if(!isEncrypt)key *= -1;
	return key;
}

static bool encryptOrDecryptInput()
{
	char encOrDec[256];
	for(;;)
	{
		printf("Encrypt or decrypt (e/d): ");
		fgets(encOrDec, 256, stdin);
		if(encOrDec[0] == 'e')return true;
		if(encOrDec[0] == 'd')return false;
		printf("Invalid input\n");
	}
}

int main() {
	bool isEncrypt = encryptOrDecryptInput();
	char* message;
	for(;;)
	{
		printf("Enter your message: ");
		fgets(message, 512, stdin);
		if(!(message == NULL || message[0] == '\n'))break;
		printf("Enter a value\n");
	}

	message = charArrayToUpper(message);

	int key;
	char keyChars[128];
	for(;;)
	{
		printf("Enter the key: ");
		fgets(keyChars, 128, stdin);
		key = atoi(keyChars);
		if(key)
		{
			break;
		}
		printf("Enter a number that isn't 0\n");
	}
	printf("Using key %d\n",key);
	key = processKey(key, isEncrypt);

	unsigned int i = 0;
	while(message[i] != 0) {
		if(message[i] >= 64 && message[i] <= 90)
		{
			message[i] += key;
			while(message[i] > 90){
				message[i] -= 26;
			}
			while(message[i] < 64){
				message[i] += 26;
			}
		}

		printf("%c", message[i]);
		i++;
	}
	printf("\n");

	return 0;
}
